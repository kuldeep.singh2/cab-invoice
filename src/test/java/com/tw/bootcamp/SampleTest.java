package com.tw.bootcamp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SampleTest {
    @Test
    void shouldRun() {
        assertEquals(4, new Sample().add(2, 2));
    }
}
