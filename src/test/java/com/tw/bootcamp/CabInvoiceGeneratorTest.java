package com.tw.bootcamp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CabInvoiceGeneratorTest {

    @Test
    void shouldCalculateFareForDistanceInKM() {
        double distanceTravelledInKM = 1;
        double totalPrice = new CabInvoiceGenerator().getFareForDistanceInKM(distanceTravelledInKM);
        assertEquals(10.0, totalPrice);
    }

    @Test
    void shouldCalculateFareForDistanceForFourKMs() {
        double distanceTravelledInKM = 4;
        double totalPrice = new CabInvoiceGenerator().getFareForDistanceInKM(distanceTravelledInKM);
        assertEquals(40.0, totalPrice);
    }

    @Test
    void shouldCalculateFareForTimeInMinutes() {
        double travelTimeInMinutes = 1;
        double totalPrice = new CabInvoiceGenerator().getFareForTimeInMinutes(travelTimeInMinutes);
        assertEquals(1.0, totalPrice);
    }

    @Test
    void shouldCalculateFareForTimeInFourAndHalfMinutes() {
        double travelTimeInMinutes = 4.5;
        double totalPrice = new CabInvoiceGenerator().getFareForTimeInMinutes(travelTimeInMinutes);
        assertEquals(4.5, totalPrice);
    }

    @Test
    void shouldCalculateFareForOneKmAndOneMinute() {
        double distanceTravelledInKM = 1;
        double travelTimeInMinutes = 1;
        double totalPrice = new CabInvoiceGenerator().calculateFare(distanceTravelledInKM, travelTimeInMinutes);
        assertEquals(16.0, totalPrice);
    }

    @Test
    void shouldCalculateMinimumFare() {
        double distanceTravelledInKM = 0;
        double travelTimeInMinutes = 0;
        double totalPrice = new CabInvoiceGenerator().calculateFare(distanceTravelledInKM, travelTimeInMinutes);
        assertEquals(5.0, totalPrice);
    }

    @Test
    void shouldCalculateFareForFourKmAndFiveMinutes() {
        double distanceTravelledInKM = 4;
        double travelTimeInMinutes = 5;
        double totalPrice = new CabInvoiceGenerator().calculateFare(distanceTravelledInKM, travelTimeInMinutes);
        assertEquals(50.0, totalPrice);
    }

    @Test
    void shouldCalculateFareForFourAndHalfKmAndFiveAndHalfMinutes() {
        double distanceTravelledInKM = 4.5;
        double travelTimeInMinutes = 5.5;
        double totalPrice = new CabInvoiceGenerator().calculateFare(distanceTravelledInKM, travelTimeInMinutes);
        assertEquals(55.5, totalPrice);
    }

    @Test
    public void shouldCalculateFareForTrip() {
        double distanceTravelledInKMs = 4.5;
        double travelTimeInMinutes = 5.5;
        Trip trip = new Trip(distanceTravelledInKMs,travelTimeInMinutes);

        double totalPrice = new CabInvoiceGenerator().calculateFareForTrip(trip);

        assertEquals(55.5, totalPrice);
    }

    @Test
    public void shouldCalculatAggregateFareForMultipleTrips() {
        Trip tripOne = new Trip(4,5);
        Trip tripTwo = new Trip(4.5,5.5);

        double totalPrice = new CabInvoiceGenerator().calculateFareForTrips(Arrays.asList(tripOne,tripTwo));

        assertEquals(105.5,totalPrice);
    }
}
