package com.tw.bootcamp;

public class Trip {
    private double distanceTravelledInKMs;
    private double timeInMinutes;

    public Trip(double distanceTravelledInKMs, double timeInMinutes) {
        this.distanceTravelledInKMs = distanceTravelledInKMs;
        this.timeInMinutes = timeInMinutes;
    }

    public double getDistanceTravelledInKMs() {
        return distanceTravelledInKMs;
    }

    public double getTimeInMinutes() {
        return timeInMinutes;
    }
}
