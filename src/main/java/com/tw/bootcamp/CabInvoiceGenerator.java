package com.tw.bootcamp;


import java.util.List;

public class CabInvoiceGenerator {

    private static final double RATE_PER_KM = 10.0;
    private static final double RATE_PER_MINUTES = 1.0;
    private static final double MINIMUM_FARE = 5.0;

    public double getFareForDistanceInKM(double distanceTravelled) {
        return RATE_PER_KM * distanceTravelled;
    }

    public double getFareForTimeInMinutes(double travelTimeInMinutes) {
        return travelTimeInMinutes * RATE_PER_MINUTES;
    }

    public double calculateFare(double distanceTravelledInKM, double travelTimeInMinutes) {
        return MINIMUM_FARE +
                getFareForDistanceInKM(distanceTravelledInKM) +
                getFareForTimeInMinutes(travelTimeInMinutes);
    }

    public double calculateFareForTrip(Trip trip) {
        return calculateFare(trip.getDistanceTravelledInKMs(), trip.getTimeInMinutes());
    }

    public double calculateFareForTrips(List<Trip> trips) {
        double totalFare = 0.0;
        for (Trip trip : trips) {
            totalFare += calculateFareForTrip(trip);
        }
        return totalFare;
    }
}
